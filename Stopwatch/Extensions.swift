//
//  Extensions.swift
//  Stopwatch
//
//  Created by Will Gunby on 02/11/2014.
//  Copyright (c) 2014 Will Gunby. All rights reserved.
//

import Foundation


extension Int {
    func format(f: String) -> String {
        return NSString(format: "%\(f)d", self)
    }
}
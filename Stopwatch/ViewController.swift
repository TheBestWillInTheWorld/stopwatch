//
//  ViewController.swift
//  Stopwatch
//
//  Created by Will Gunby on 02/11/2014.
//  Copyright (c) 2014 Will Gunby. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var lblTimer: UILabel!
    var timer : Stopwatch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timer = Stopwatch(setText)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnStart_up(sender: UIBarButtonItem) { timer.start() }
    @IBAction func btnStop_up (sender: UIBarButtonItem) { timer.pause() }
    @IBAction func btnClear_up(sender: UIBarButtonItem) { timer.reset() }
    
    func setText(milliseconds:Int,seconds:Int,minutes:Int){
        var minSecFormat = "02"
        var millisecFormat = "03"
        lblTimer.text = "\(minutes.format(minSecFormat)):\(seconds.format(minSecFormat)):\(milliseconds.format(millisecFormat))"
    }
}
